from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views import View

from .models import Task, Item
import json

class TaskView(View):
    model = Task
    
    def get(self, request):
        """
        List all
        """
        task_list = []
        item_dict = {}
        # set default
        limit = 10
        offset = 0
        if  'limit' in request.GET and 'offset' in request.GET:
            limit = int(request.GET.get('limit'))
            offset = int(request.GET.get('offset'))

        qs = Task.objects.all().prefetch_related("item_set") #coba taro [offset:(offset+limit)] disini
        total = len(qs)
        for i in qs:
            task_list.extend([{'id': i.id, 'name': i.name, 'description': i.description, 'item': []}])
            for j in i.item_set.all():
                task_list[i.id - 1]['item'].append({'id': j.id, 'name': j.name})
        tasks = list(task_list[offset:(offset + limit)])
        
        previous_link = self.get_previous_link(limit, offset)
        next_link = self.get_next_link(limit, offset, total)
        
        data = {
            "message": "Successful",
            "total": total,
            "count": len(tasks),
            "status": 200,
            "data": tasks,
            "previous": previous_link,
            "next": next_link,
        }
        return JsonResponse(data, safe=False)
    
    def get_previous_link(self, limit, offset):
        previous_number = offset - limit
        if previous_number < 0:
            previous_number = 0
        previous_link = None
        if offset != 0 or previous_number != 0:
            previous_link = 'http://127.0.0.1:8002/tasks/api/?limit=' +str(limit)+ '&offset=' + str(previous_number)
        return previous_link
    
    def get_next_link(self, limit, offset, total):
        next_number = offset + limit
        next_link = 'http://127.0.0.1:8002/tasks/api/?limit=' +str(limit)+ '&offset=' + str(next_number)
        if next_number >= total:
            next_link = None
        return next_link
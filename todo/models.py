from django.db import models

# Create your models here.
class Task(models.Model):
    """
    Class Task
    """
    name = models.CharField(max_length=250, null=True, blank=True)
    description = models.CharField(max_length=250, null=True, blank=True)
    
    class Meta:
        ordering = ['id']
    
    def __str__(self):
        return self.name
    
class Item(models.Model):
    """
    Class item todo
    """
    name = models.CharField(max_length=250, null=True, blank=True)
    task = models.ForeignKey('Task',  on_delete=models.CASCADE, null=True, blank=True)
    
    def __str__(self):
        return self.name

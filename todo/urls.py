from django.urls import path
from . import views

app_name = 'todo'

urlpatterns = [
    path('', views.TaskView.as_view())
    # path('', views.post_list, name='post_list'),
    # path('category/', views.category_list, name='category_list'),
    # path('tagged/', views.tag_list, name='tag_list'),
]
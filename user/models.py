from django.db import models
from django.contrib.auth.models import AbstractUser

class CustomUser(AbstractUser):
    ''' 
    Create Custom user for further use later
    '''
    bio = models.CharField(max_length=250)
    
    def __str__(self):
        return self.username